<?php
/**
 * Created by PhpStorm.
 * User: e7c_corp
 * Date: 2019/1/8
 * Time: 15:54
 */

namespace E7cCorp\EasyTBK\JingDong;


interface RequestInterface
{
    public function getMethod();

    public function getParamJson();
}