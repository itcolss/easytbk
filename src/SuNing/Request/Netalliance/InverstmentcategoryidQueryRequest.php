<?php
namespace E7cCorp\EasyTBK\SuNing\Request\Netalliance;

use E7cCorp\EasyTBK\SuNing\SuningRequest;
use E7cCorp\EasyTBK\SuNing\RequestCheckUtil;

/**
 * 苏宁开放平台接口 -
 *
 * @author suning
 * @date   2019-11-6
 */
class InverstmentcategoryidQueryRequest  extends SuningRequest{

	public function getApiMethodName(){
		return 'suning.netalliance.inverstmentcategoryid.query';
	}

	public function getApiParams(){
		return $this->apiParams;
	}

	public function check(){
		//非空校验
	}

	public function getBizName(){
		return "queryInverstmentcategoryid";
	}

}

?>
