<?php
/**
 * Created by PhpStorm.
 * User: e7c_corp
 * Date: 2019/1/8
 * Time: 15:43
 */

namespace E7cCorp\EasyTBK\PinDuoDuo;


interface RequestInterface
{
    public function getParams();
}
