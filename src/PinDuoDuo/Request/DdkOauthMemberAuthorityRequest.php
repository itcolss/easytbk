<?php
namespace E7cCorp\EasyTBK\PinDuoDuo\Request;

use E7cCorp\EasyTBK\PinDuoDuo\RequestInterface;


class DdkOauthMemberAuthorityRequest implements RequestInterface
{
    /**
     * 查询多多进宝商品详情
     * @var string
     */
    private $type = 'pdd.ddk.oauth.member.authority.query';

    /**
     * 商品ID，仅支持单个查询。例如：[123456]
     * @var
     */
    private $pid;

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setPid($pid)
    {
        $this->pid = $pid;
    }


    public function getType()
    {
        return $this->type;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function getParams()
    {
        return [
            'type'          => $this->type,
            'pid'          => $this->pid,
        ];
    }
}